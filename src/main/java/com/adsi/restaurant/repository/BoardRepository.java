package com.adsi.restaurant.repository;

import com.adsi.restaurant.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<Board, Integer> {
}
