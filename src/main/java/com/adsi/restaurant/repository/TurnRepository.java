package com.adsi.restaurant.repository;

import com.adsi.restaurant.domain.Turn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TurnRepository extends JpaRepository<Turn, Integer> {
}
