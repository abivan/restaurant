package com.adsi.restaurant.service.imp;

import com.adsi.restaurant.domain.Turn;
import com.adsi.restaurant.repository.TurnRepository;
import com.adsi.restaurant.service.TurnService;
import com.adsi.restaurant.service.dto.TurnDto;
import com.adsi.restaurant.service.exception.ObjectError;
import com.adsi.restaurant.service.exception.ObjectException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TurnServiceImp implements TurnService {

    @Autowired
    TurnRepository repository;

    public static final ModelMapper modelMapper = new ModelMapper();

    @Override
    public TurnDto create(TurnDto turnDto) {
        if (turnDto.getName().equals("")) {
            throw new ObjectException("El nombre no debe estar vacio",
                    new ObjectError(HttpStatus.BAD_REQUEST, "El campo nombre esta vacio"));
        }
        return modelMapper.map(repository.save(modelMapper.map(turnDto, Turn.class)), TurnDto.class);
    }

    @Override
    public Page<TurnDto> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(turn -> modelMapper.map(turn, TurnDto.class));
    }

    @Override
    public TurnDto findById(Integer id) {
        Optional<Turn> optionalTurn = repository.findById(id);
        if (!optionalTurn.isPresent()){
            throw new ObjectException("El turno no fue encontrado",
                    new ObjectError(HttpStatus.NOT_FOUND,
                            "el turno con id:"+ id + " no fue encontrado"));
        }
        return modelMapper.map(optionalTurn.get(),TurnDto.class);
    }
}
