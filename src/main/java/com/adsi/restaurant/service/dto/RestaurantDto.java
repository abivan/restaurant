package com.adsi.restaurant.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class RestaurantDto implements Serializable {
    @Id
    private int id;

    private String name;
    private String description;
    private String address;

    @Lob
    private byte[] image;
}
