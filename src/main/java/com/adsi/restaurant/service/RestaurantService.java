package com.adsi.restaurant.service;

import com.adsi.restaurant.service.dto.RestaurantDto;

import java.util.List;

public interface RestaurantService {

    List<RestaurantDto> findAll();

    RestaurantDto findById(Integer id);
}
