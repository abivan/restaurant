package com.adsi.restaurant.service;

import com.adsi.restaurant.service.dto.TurnDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TurnService {
    TurnDto create(TurnDto turnDto);
    Page<TurnDto> findAll(Pageable pageable);
    TurnDto findById(Integer id);
}
