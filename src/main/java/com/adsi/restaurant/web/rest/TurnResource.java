package com.adsi.restaurant.web.rest;

import com.adsi.restaurant.service.TurnService;
import com.adsi.restaurant.service.dto.TurnDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/turn")
public class TurnResource {

    @Autowired
    TurnService service;

    @PostMapping("/")
    public ResponseEntity<TurnDto> create(@RequestBody TurnDto turnDto) {
        /*
        Map<String, Object> response = new HashMap<>();
        if(result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo" + e.getField() + "= "+ e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        */
        return new ResponseEntity<>(service.create(turnDto), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Page<TurnDto>> findAll(@PageableDefault(page=0, size = 10) Pageable pageable){
        return new ResponseEntity<>(service.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TurnDto> findById(@PathVariable Integer id){
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

}
